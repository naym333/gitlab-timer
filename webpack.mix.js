let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath('public');
mix.js('asset/app.js', 'public/asset/js')
    .autoload({
        jquery: ['$', 'window.jQuery']
    }).vue()

    .sass('asset/app.scss', 'public/asset/css')
    .sourceMaps();
